export default interface formTable {
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  address: string;
  city: string;
  state: string;
  //metric
  metricInsitute: string;
  metricMajors: string;
  metricTotalMarks: string;
  metricObtainMarks: string;
  metricPercentage: string;
  //College
  collegeInsitute: string;
  collegeMajors: string;
  collegeTotalMarks: string;
  collegeObtainMarks: string;
  collegePercentage: string;
  //uni
  uniInsitute: string;
  degree: string;
  cgpa: string;
}
