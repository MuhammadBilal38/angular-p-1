import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from '../register.service';
import { ActivatedRoute } from '@angular/router';
import { GetDataService } from '../get-data.service';
import { UpdateDataService } from '../update-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-information-form',
  templateUrl: './information-form.component.html',
  styleUrls: ['./information-form.component.css']
})
export class InformationFormComponent implements OnInit {
  person_id;
  person_record;
  edit_status;
  _formBuilder;

  constructor(
    private _registerService: RegisterService,
    private fb: FormBuilder,
    private _activatedRoute: ActivatedRoute,
    private _getData: GetDataService,
    private _upgateData: UpdateDataService,
    private _router: Router
  ) {
    this.edit_status = this._activatedRoute.snapshot.paramMap.get('status');
    if (this.edit_status) {
      this.person_id = this._activatedRoute.snapshot.paramMap.get('id');
      this._getData.getOneData(this.person_id).subscribe(
        response => {
          console.log(response);
          this.setResponse(response);
        },
        error => console.log('error', error)
      );
    }
  }

  ngOnInit(): void {}

  //Personal Information Getter
  get firstname() {
    return this.informationHolder.get('firstname');
  }
  get lastname() {
    return this.informationHolder.get('lastname');
  }
  get email() {
    return this.informationHolder.get('email');
  }
  get phone() {
    return this.informationHolder.get('phone');
  }
  get address() {
    return this.informationHolder.get('address');
  }
  get city() {
    return this.informationHolder.get('city');
  }
  get gender() {
    return this.informationHolder.get('gender');
  }

  //getter for metric information
  get insituteMetric() {
    return this.informationHolder.get('insituteMetric');
  }
  get majorMetric() {
    return this.informationHolder.get('majorMetric');
  }
  get totalmarksMetric() {
    return this.informationHolder.get('totalmarksMetric');
  }
  get obtainmarksMetric() {
    return this.informationHolder.get('obtainmarksMetric');
  }
  get percentageMetric() {
    return this.informationHolder.get('percentageMetric');
  }

  //get fo College Information Input
  get insituteCollege() {
    return this.informationHolder.get('insituteCollege');
  }
  get majorCollege() {
    return this.informationHolder.get('majorCollege');
  }
  get totalmarksCollege() {
    return this.informationHolder.get('totalmarksCollege');
  }
  get obtainmarksCollege() {
    return this.informationHolder.get('obtainmarksCollege');
  }
  get percentageCollege() {
    return this.informationHolder.get('percentageCollege');
  }

  //Getter for Uni Information
  get insituteUni() {
    return this.informationHolder.get('insituteUni');
  }
  get degree() {
    return this.informationHolder.get('degree');
  }
  get cgpa() {
    return this.informationHolder.get('cgpa');
  }

  //Binding Input Fields With Form And Applyinf Validators

  informationHolder = this.fb.group({
    firstname: ['', [Validators.required, Validators.minLength(3)]],
    lastname: ['', [Validators.required, Validators.minLength(3)]],
    email: ['', [Validators.required, Validators.email]],
    phone: [
      '',
      [Validators.required, Validators.maxLength(11), Validators.minLength(11)]
    ],
    address: ['', [Validators.required]],
    city: ['', [Validators.required]],
    gender: ['', [Validators.required]],
    // mertic
    insituteMetric: ['', [Validators.required]],
    majorMetric: ['', [Validators.required]],
    totalmarksMetric: ['', [Validators.required, Validators.min(0)]],
    obtainmarksMetric: ['', [Validators.required, Validators.min(0)]],
    percentageMetric: [
      '',
      [Validators.required, Validators.max(100), Validators.min(0)]
    ],
    // College
    insituteCollege: ['', [Validators.required]],
    majorCollege: ['', [Validators.required]],
    totalmarksCollege: ['', [Validators.required, Validators.min(0)]],
    obtainmarksCollege: ['', [Validators.required, Validators.min(0)]],
    percentageCollege: [
      '',
      [Validators.required, Validators.max(100), Validators.min(0)]
    ],
    // Uni
    insituteUni: ['', [Validators.required]],
    degree: ['', [Validators.required]],
    cgpa: ['', [Validators.required, Validators.max(4), Validators.min(0)]]
  });

  setResponse(response) {
    this.informationHolder.setValue({
      firstname: response.firstname,
      lastname: response.lastname,
      email: response.email,
      address: response.address,
      city: response.city,
      phone: response.phone,
      gender: response.gender,
      //Metric
      insituteMetric: response.insituteMetric,
      majorMetric: response.majorMetric,
      totalmarksMetric: response.totalmarksMetric,
      obtainmarksMetric: response.obtainmarksMetric,
      percentageMetric: response.percentageMetric,
      // College
      insituteCollege: response.insituteCollege,
      majorCollege: response.majorCollege,
      totalmarksCollege: response.totalmarksCollege,
      obtainmarksCollege: response.obtainmarksCollege,
      percentageCollege: response.percentageCollege,
      // Uni
      insituteUni: response.insituteUni,
      degree: response.degree,
      cgpa: response.cgpa
    });
  }

  onSubmit() {
    if (this.informationHolder.valid) {
      if (this.edit_status) {
        this._upgateData.update(this.person_id, this.informationHolder.value);

        alert('the information is successfully updated');
        this._router.navigate(['']);
      } else {
        this._registerService.createperson(this.informationHolder.value);

        // this._registerService.register(this.informationHolder.value).subscribe(
        //   response => console.log('sucess'),
        //   error => console.log('error')
        // );
        alert('the Information is successfully added');
      }
      this.informationHolder.reset();
    } else {
      alert('Please Fill the required Fields');
    }
  }
}
