import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-card-layout',
  templateUrl: './card-layout.component.html',
  styleUrls: ['./card-layout.component.css']
})
export class CardLayoutComponent implements OnInit {
  recods = [];

  constructor(private _getData: GetDataService, private _router: Router) {
    this._getData.getData().subscribe(data => {
      for (let i = 0; i < data.length; i++) {
        this.recods.push(data[i].payload);
      }
    });
  }

  ngOnInit(): void {}

  onProfileClick(id) {
    this._router.navigate(['/profile', id]);
  }
}
