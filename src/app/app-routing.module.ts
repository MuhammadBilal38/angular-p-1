import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InformationFormComponent } from './information-form/information-form.component';
import { TableViewComponent } from './table-view/table-view.component';
import { ProfileComponent } from './profile/profile.component';
import { CardLayoutComponent } from './card-layout/card-layout.component';

const routes: Routes = [
  { path: '', component: InformationFormComponent },
  { path: 'form', component: InformationFormComponent },
  { path: 'table', component: TableViewComponent },
  { path: 'form/:id/:status', component: InformationFormComponent },
  { path: 'profile/:id', component: ProfileComponent },
  { path: 'cardlayout', component: CardLayoutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [
  InformationFormComponent,
  TableViewComponent,
  ProfileComponent,
  CardLayoutComponent
];
