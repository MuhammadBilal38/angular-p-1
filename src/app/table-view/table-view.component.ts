import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { DeleteDataService } from '../delete-data.service';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {
  recods = [];
  ids = [];
  edit_status = false;

  constructor(
    private _getData: GetDataService,
    private _http: HttpClient,
    private _deleteService: DeleteDataService,
    private _router: Router,
    private firestore: AngularFirestore
  ) {
    this._getData.getData().subscribe(data => {
      for (let i = 0; i < data.length; i++) {
        this.recods.push(data[i].payload);
      }
    });
  }

  ngOnInit(): void {}

  onDelete(id, thisptr) {
    /* //postgresql code
    // this._deleteService.delete(id).subscribe(
    //   response => console.log('successfuly Deleted'),
    //   err => console.log(err)
    // );*/
    let td = thisptr.target.parentNode;
    let tr = td.parentNode;
    tr.parentNode.removeChild(tr);

    //servies
    this._deleteService.delete(id);
  }

  onEdit(id) {
    this.edit_status = true;
    this._router.navigate(['/form', id, this.edit_status]);
  }
  onProfile(id) {
    this._router.navigate(['/profile', id]);
  }
}
