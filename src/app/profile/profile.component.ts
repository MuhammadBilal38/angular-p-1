import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { GetDataService } from '../get-data.service';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  //Personal Info Variables
  person_id;
  person_record;
  name;
  lastname;
  email;
  phone;
  address;
  city;
  gender;
  //metric Varaibles
  insituteMetric;
  majorsMetric;
  tmMetric;
  omMetric;
  pMetric;
  //College Variables
  insituteCollege;
  majorsCollege;
  tmCollege;
  omCollege;
  pCollege;
  //uni Variables
  insituteUni;
  degree;
  cgpa;

  constructor(
    private _getData: GetDataService,
    private _activatedRouter: ActivatedRoute
  ) {
    this.person_id = this._activatedRouter.snapshot.paramMap.get('id');
    if (this.person_id) {
      this._getData.getOneData(this.person_id).subscribe(
        response => {
          this.setResponse(response);
        },
        err => {
          console.log(err);
        }
      );
    }
  }

  ngOnInit(): void {}

  setResponse(response) {
    this.person_record = response;
    this.name = response.firstname;
    this.lastname = response.lastname;
    this.email = response.email;
    this.phone = response.phone;
    this.address = response.address;
    this.city = response.city;
    this.gender - response.gender;
    this.insituteMetric = response.insituteMetric;
    this.majorsMetric = response.majorMetric;
    this.tmMetric = response.totalmarksMetric;
    this.omMetric = response.obtainmarksMetric;
    this.pMetric = response.percentageMetric;
    this.insituteCollege = response.insituteCollege;
    this.majorsCollege = response.majorCollege;
    this.tmCollege = response.totalmarksCollege;
    this.omCollege = response.obtainmarksCollege;
    this.pCollege = response.percentageCollege;
    this.insituteUni = response.insituteUni;
    this.degree = response.degree;
    this.cgpa = response.cgpa;
  }
}
