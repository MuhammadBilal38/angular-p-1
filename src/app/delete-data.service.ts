import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DeleteDataService {
  _url = 'http://localhost:3000/delete/';
  constructor(private _http: HttpClient, private fireStore: AngularFirestore) {}

  delete(id) {
    return this.fireStore
      .collection('/course')
      .doc(id)
      .delete();

    // return this._http.delete(this._url + personId.toString());
  }
}
