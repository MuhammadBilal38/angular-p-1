import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  _url = 'http://localhost:3000/register';
  constructor(private _http: HttpClient, private fireStore: AngularFirestore) {}

  register(userData) {
    return this._http.post<any>(this._url, userData);
  }

  createperson(userdata) {
    return new Promise<any>((resolve, reject) => {
      this.fireStore
        .collection('course')
        .add(userdata)
        .then(
          res => {
            console.log(res);
          },
          err => reject(err)
        );
    });
  }
}
