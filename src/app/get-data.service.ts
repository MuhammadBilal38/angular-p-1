import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AngularFirestore } from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class GetDataService {
  _url = 'http://localhost:3000/table';
  _urlOneData = 'http://localhost:3000/form/';

  ///////

  labels;
  constructor(private _http: HttpClient, private fireStore: AngularFirestore) {}

  getData() {
    //   return this._http.get<any>(this._url);
    return this.fireStore.collection('/course').snapshotChanges();

    // console.log(this.fireStore.collection('/course').snapshotChanges());
  }

  getOneData(id) {
    return this.fireStore
      .collection('/course')
      .doc(id)
      .valueChanges();

    //   console.log(this._http.get<any>(this._urlOneData + personId));
    //   return this._http.get<any>(this._urlOneData + personId);
  }
}
