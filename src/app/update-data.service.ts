import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UpdateDataService {
  _url = 'http://localhost:3000/update/';
  constructor(private _http: HttpClient, private fireStore: AngularFirestore) {}

  update(id, userData) {
    this.fireStore
      .collection('/course')
      .doc(id)
      .update(userData);
    //return this._http.put<any>(this._url + id, userData);
  }
}
