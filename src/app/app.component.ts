import { Component } from '@angular/core';
import { FormBuilder, Validators, MinLengthValidator } from '@angular/forms';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private _registerService: RegisterService,
    private fb: FormBuilder
  ) {}
}
