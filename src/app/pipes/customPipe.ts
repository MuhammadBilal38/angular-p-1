import { Pipe, PipeTransform } from '@angular/core';
@Pipe({ name: 'NamePipe' })
export class customPipe implements PipeTransform {
  transform(fristname: string, lastname: string): string {
    return fristname + ' ' + lastname;
  }
}
