export class Person {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  address: string;
  city: string;
  gender: string;
  insituteMetric: string;
  majorMetric: string;
  totalmarksMetric: string;
  obtainmarksMetric: string;
  percentageMetric: string;
  insituteCollege: string;
  majorCollege: string;
  totalmarksCollege: string;
  obtainmarksCollege: string;
  percentageCollege: string;
  insituteUni: string;
  degree: string;
  cgpa: string;
  constructor(
    id,
    fname,
    lname,
    email,
    phone,
    address,
    cit,
    gender,
    minsitute,
    mMsjors,
    mTM,
    mOM,
    mP,
    cinsitute,
    cMsjors,
    cTM,
    cOM,
    cP,
    uinsitute,
    Degree,
    cgpa
  ) {
    this.id = id;
    this.firstname = fname;
    this.lastname = lname;
    this.email = email;
    this.phone = phone;
    this.address = address;
    this.city = cit;
    this.gender = gender;
    this.insituteMetric = minsitute;
    this.insituteCollege = cinsitute;
    this.majorMetric = mMsjors;
    this.majorCollege = cMsjors;
    this.totalmarksMetric = mTM;
    this.obtainmarksMetric = mOM;
    this.totalmarksCollege = cTM;
    this.obtainmarksCollege = cOM;
    this.percentageMetric = mP;
    this.percentageCollege = cP;
    this.insituteUni = uinsitute;
    this.degree = Degree;
    this.cgpa = cgpa;
  }
}
