// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDTsEAsJfpz2C1vh4_jy6Nz-RUMQf-jRB4',
    authDomain: 'informationholder-957d7.firebaseapp.com',
    databaseURL: 'https://informationholder-957d7.firebaseio.com',
    projectId: 'informationholder-957d7',
    storageBucket: 'informationholder-957d7.appspot.com',
    messagingSenderId: '721307755032',
    appId: '1:721307755032:web:9455381a3867a3bea6d96d',
    measurementId: 'G-ZBVFNET771'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
